# reactJS-firebase-test

There are two directory for admin and client. Please follow the follwoing step to see the functionality of the project.

1. Goto the 'airvat-admin-db-seed' directory using cd airvat-admin-db-seed.

2. Follow the README.md file instructions in airvat-admin-db-seed.

3. Open new terminal and goto the 'client-react-firbase' directory.

4. Follow the README.md file instructions in client-react-firbase.


