import Moment from 'moment-timezone';

export const standardDateTimeFormatter = timestamp => {
    return  Moment.unix(timestamp/1000).format("YYYY-MM-DD");
}


export default  standardDateTimeFormatter;