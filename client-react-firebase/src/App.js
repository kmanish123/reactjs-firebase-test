import React, { Component } from 'react';
import './App.css';
import Navbar from './components/header/navbar';
import Dashboard from './components/dashboard/dashboard';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div className="">
      <Navbar/>
      <Dashboard/>
      </div>
    );
  }
}

export default App;