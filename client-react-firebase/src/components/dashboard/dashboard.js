import React, { Component } from 'react';
import firebase from '../../Firebase';
import Loader from '../loader/loader';
import  standardDateTimeFormatter  from '../../utils/dates';
import DatePicker from "react-datepicker"; 
import Moment from 'moment-timezone';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      loading:true,
      sorticon:'fa fa-sort-amount-asc',
      sortBy:'firstName',
      orderBy:1,
      startDate: new Date(),
      endDate: new Date()
    };
    this.startDatehandleChange = this.startDatehandleChange.bind(this);
    this.endDatehandleChange = this.endDatehandleChange.bind(this);
    this.fetchUsers = this.fetchUsers.bind(this);
  }

// Startdate for date filter
  startDatehandleChange(date) {
    this.setState({
      startDate: date
    });
  }

  // Enddate for date filter
  endDatehandleChange(date) {
    this.setState({
      endDate: date
    });
  }

// Fetch all users from users collection and account documnet
fetchUsers(self) {
  console.log("self.state.sortBy",self.state.sortBy);
    return new Promise(function(resolve, reject){
        firebase.database().ref('users/').orderByChild(`account/${self.state.sortBy}`).on('value', function (snapshot) {
          const users = [];
            if (snapshot) {
                snapshot.forEach((user) => {
                    const {lastActive, account } = user.val();
                           users.push({
                            key: user.key,
                            user, // DocumentSnapshot,
                            account,
                            lastActive
                          });
                 });
                 if(self.state.orderBy === -1){
                  let reverarray = users.reverse();
                  self.setState({
                   users: reverarray,
                   loading:false
                   });
                }
                else{
                 self.setState({
                   users: users,
                   loading:false
                   });
                  }
                resolve(users);
              } else {
                reject('err');
              }
        }); 
     });
}

// Filter users data based on there firstname, lastname, email, country, or city.

filter = (e) => {
  const self = this
  const text = e.target.value.toUpperCase();
  this.setState({
    startDate: new Date(),
    endDate: new Date()
  });
  return new Promise(function(resolve, reject){
  firebase.database().ref('users')
   .orderByChild(`account/${e.target.name}`)
   .startAt(text)
    .endAt(text+"\uf8ff")
    .once("value", function(snapshot){
    const users = [];
    console.log("snapshot", snapshot)
    if (snapshot) {
        snapshot.forEach((user) => {
            const {lastActive, account } = user.val();
                   users.push({
                    key: user.key,
                    user, // DocumentSnapshot,
                    account,
                    lastActive
                  });
         });
         if(self.state.orderBy === -1){
          let reverarray = users.reverse();
          self.setState({
           users: reverarray,
           loading:false
           });
        }
        else{
         self.setState({
           users: users,
           loading:false
           });
          }
        resolve(users);
      } 
    else{
      reject('err');
    }
  })

})
}

//Filter date range function with startdata and enddate.
filterByDate = (e) => {
  const self = this
  const {startDate, endDate} = this.state;
   const momnetstart = new Moment(startDate).valueOf();
   const momnetend = new Moment(endDate).valueOf();
  return new Promise(function(resolve, reject){
  firebase.database().ref('users')
   .orderByChild(`lastActive`)
   .startAt(momnetstart)
    .endAt(momnetend+"\uf8ff")
    .on("value", function(snapshot){
    const users = [];
    if (snapshot) {
        snapshot.forEach((user) => {
            const {lastActive, account } = user.val();
                   users.push({
                    key: user.key,
                    user, // DocumentSnapshot,
                    account,
                    lastActive
                  });
         });
         if(self.state.orderBy === -1){
          let reverarray = users.reverse();
          self.setState({
           users: reverarray,
           loading:false
           });
        }
        else{
         self.setState({
           users: users,
           loading:false
           });
          }
        resolve(users);
      } 
    else{
      reject('err');
    }
  })

})
}

 
// Sorting for all fields on in ascending and descending order.
customSort = (sortValue, event) => {
  const self = this
  if(this.state.sorticon === 'fa fa-sort-amount-asc'){
    self.setState({
      sorticon : 'fa fa-sort-amount-desc',
      sortBy: sortValue,
      orderBy:-1
    },()=>{
      this.sortFunction(sortValue);
    })
  }
  else{
    self.setState({
      sorticon : 'fa fa-sort-amount-asc',
      sortBy: sortValue,
      orderBy:1
    }, ()=>{
      this.sortFunction(sortValue);
    })
  }
}


sortFunction = (sortValue) =>{
    let self = this;
    let ref =  firebase.database().ref('users').orderByChild(`account/${sortValue}`);
    if(sortValue === 'lastActive'){
      ref =  firebase.database().ref('users').orderByChild(`${sortValue}`);
    }
    this.setState({
      startDate: new Date(),
      endDate: new Date()
    },()=>{
      return new Promise(function(resolve, reject){
        ref.once("value", function(snapshot){
         const users = [];
         if (snapshot) {
             snapshot.forEach((user) => {
                 const {lastActive, account } = user.val();
                        users.push({
                         key: user.key,
                         user, // DocumentSnapshot,
                         account,
                         lastActive
                       });
              });
              if(self.state.orderBy === -1){
                let reverarray = users.reverse();
                self.setState({
                 users: reverarray,
                 loading:false
                 });
              }
              else{
               self.setState({
                 users: users,
                 loading:false
                 });
              }
              
             resolve(users);
           } 
         else{
           reject('err');
         }
       })
     })
    });
  }


  componentWillMount() {
      this.fetchUsers(this).then(users =>{console.log("users from firbase")}); 
  }


  render() {
    const { startDate ,endDate} = this.state;
    var momnetstart = new Moment(startDate).valueOf();
     var momnetend = new Moment(endDate).valueOf();
     let isDisable = momnetstart > momnetend ? true : false;
    if (this.state.loading === true) {
        return <Loader />
    }

    return (
        <div className="card card-default">
          <div className="dasboard-body">
          <h1>Dashboard Data Filter & Sorting</h1>
          <div className="table-responsive">
            <table className="table table-striped table-bordered">
              <thead>
              <tr className="filtertd">
                  <th><input type={"text"} name="firstName"  onChange={this.filter} className="form-control"/></th>
                  <th><input type={"text"} name="surname" onChange={this.filter} className="form-control"/></th>
                  <th><input type={"text"} name="email" onChange={this.filter} className="form-control"/></th>
                  <th><input type={"text"} name="residenceCountry" onChange={this.filter} className="form-control"/></th>
                  <th><input type={"text"} name="residenceCity" onChange={this.filter} className="form-control"/></th>
                  <th>
                <div className="">
                <div className="d-flex">
                 <DatePicker className="startdate" dateFormat="YYYY-MM-dd" name="startDate"  selected={this.state.startDate} onChange={this.startDatehandleChange}/>
                  <span>to</span>
                 <DatePicker className="enddate"popperPlacement="bottom-end"  dateFormat="YYYY-MM-dd" name="endDate"  selected={this.state.endDate} onChange={this.endDatehandleChange} style={{float:'left'}}/>
                </div>
                <div className="d-flex">
                  <button onClick={this.filterByDate} disabled={isDisable} className="btn btn-primary btn-small disbaled-style">Filter</button>
                  <button onClick={this.customSort.bind(this,'lastActive')} className="btn btn-secondary btn-small">Reset</button>
                  </div>       
                </div>
                </th>
                </tr>
        
                <tr>
                  <th onClick={this.customSort.bind(this,'firstName')}>First Name <i className={this.state.sortBy!=='firstName'? 'fa fa-sort-amount-asc': this.state.sorticon} style={{color:this.state.sortBy!=='firstName' ? '#808080':'#000000'}}></i></th>
                  <th onClick={this.customSort.bind(this,'surname')}>Last Name <i className={this.state.sortBy!=='surname'? 'fa fa-sort-amount-asc': this.state.sorticon} style={{color:this.state.sortBy!=='surname' ? '#808080':'#000000'}} ></i></th>
                  <th onClick={this.customSort.bind(this,'email')}>Email <i className={this.state.sortBy!=='email'? 'fa fa-sort-amount-asc': this.state.sorticon} style={{color:this.state.sortBy!=='email' ? '#808080':'#000000'}}></i></th>
                  <th onClick={this.customSort.bind(this,'residenceCountry')}>Country <i className={this.state.sortBy!=='residenceCountry'? 'fa fa-sort-amount-asc': this.state.sorticon} style={{color:this.state.sortBy!=='residenceCountry' ? '#808080':'#000000'}}></i></th>
                  <th onClick={this.customSort.bind(this,'residenceCity')}>City <i className={this.state.sortBy!=='residenceCity'? 'fa fa-sort-amount-asc': this.state.sorticon} style={{color:this.state.sortBy!=='residenceCity' ? '#808080':'#000000'}}></i></th>
                  <th onClick={this.customSort.bind(this,'lastActive')}>Date <i className={this.state.sortBy!=='lastActive'? 'fa fa-sort-amount-asc': this.state.sorticon} style={{color:this.state.sortBy!=='lastActive' ? '#808080':'#000000'}}></i></th>
                </tr>
              </thead>
              <tbody>
                {this.state.users.map(user =>
                  <tr key={user.key}>
                    <td>{user.account.firstName}</td>
                    <td>{user.account.surname}</td>
                    <td>{user.account.email}</td>
                    <td>{user.account.residenceCountry}</td>
                    <td>{user.account.residenceCity}</td>
                    <td>{standardDateTimeFormatter(user.lastActive)}</td>
                  </tr>
                )}
              </tbody>
            </table>
        </div>
          </div>
        </div>
    );
  }
}

export default Dashboard;