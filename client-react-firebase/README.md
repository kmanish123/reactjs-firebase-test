# client-react-firbase

This repository will show you firebase realtime users data with with filter for each field, filter based on the date range and sorting in ascending and descending order. 

1. Once cloned, run ```npm i``` in the root directory. 

2. Update the Firebase.js file with your credentials from firebase appConfig. 

4. Run ```npm start``` to start the project on browser.



# The follwing task are accomplished by this project.

The widget should encompass a table with the following columns: 
First Name
Last Name
Email
Phone No.
Residence Country
Residence City
Last Active

 

The resulting table should allow to handle multiple filters at a time: 
Filter by a user first name (case insensitive)
Filter by a user last name (case insensitive)
Filter by a user email (case insensitive)
Filter by a user residence country (case insensitive)
Filter by a user residence City (case insensitive)
Filter by a range of Last Active ( start date / end date) 
Sort the result by any of these fields ascending/descending 